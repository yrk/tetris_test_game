#ifndef TETRIS_OBJECT_H
#define TETRIS_OBJECT_H

#include "EngineObject.h"

class CTetrisObject : public CEngineObject
{
protected:

public:
	CTetrisObject(void);
	~CTetrisObject(void);

	//void set_sbs(GLfloat _sbs);

	// function to call from CGameEngine
	virtual void draw() = 0;
};

#endif
