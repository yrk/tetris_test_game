#ifndef BRICK_H
#define BRICK_H

#include "custom_types.h"

#include <windows.h>
#include <gl/gl.h>
#include <iostream>

#include "TetrisObject.h"
//#include "Well.h"
class CWell;

enum TOrientation {
		TO_NORMAL = 0,
		TO_RIGHT,
		TO_DOWN,
		TO_LEFT,
		TO_SIZE
};

typedef struct CColor {
	GLfloat r,g,b;
} CColor;

struct TBrick {
	unsigned int tall, wide;

};

// small rectangular cube from wich are formed all bricks
typedef struct TCubeBrick {
	CColor color;


} TCubeBrick;

class CBrick : public CTetrisObject
{

protected:

	//COORD pos; // logical position of brick
	int x, y; // logical position of brick in a well
	GLfloat glx, gly; // openGL position of brick
	TOrientation current_orient;

	unsigned int shadow_x, shadow_y;
	GLfloat shadow_glx, shadow_gly;

	bool _close_to_left_wall;
	bool _close_to_right_wall;

	CColor color;
	
	bool new_bricks[2][16];

	int brick_type;

	float sbs, dsbs;
	float brick_space;

	bool ** current_mask; // matrix of current position/collision
	bool *** layouts;

	CWell * well; // well managing this brick

	//CBrick * shadow;
	
	//void rotate_matrix33(bool ** src_mat, bool ** dest_mat);
	void rotate_matrix33(bool src_mat[3][3], bool dest_mat[3][3]);
	void rotate_matrix44(bool src_mat[4][4], bool dest_mat[4][4]);

	void _alloc_layout_mem();
	//void _alloc_layout_mem_set(bool newLayout[4][3][3]);
	void _alloc_layout_mem_set(bool newLayout[4][4][4]);
	void _free_layout_mem();

	void close_to_wall_update();

public:
	CBrick(CWell * my_well);
	//CBrick(CWell * my_well, bool ** brick_type = NULL);
	//CBrick(CWell * my_well, bool brick_type[3][3] = NULL);
	~CBrick(void);

	bool close_to_left_wall() { return _close_to_left_wall; }
	bool close_to_right_wall() { return _close_to_right_wall; }

	unsigned int texture;
	int current_color;

	//void load_layouts(bool newLayout[4][3][3]);
	void load_layouts(bool newLayout[4][4][4]);
	// fills brick_mask from param matrix with proper matrix rotations
	void create_rotation_matrix33(bool ** new_brick);

	void draw_simple();

	void draw();
	void set_xy(unsigned int _x, unsigned int _y);
	void rotate(TOrientation orient);
	void move(TOrientation dest);

	//void load_brick(bool ** brick_type);
	//void load_brick(bool brick_type[3][3]);
	void load_brick(bool brick_type[4][4]);
	void load_brick();

	void load_brick(int rand_type);
	int get_brick_type() { return brick_type; }

	void makeSmallBrick();
	void makeSmallBrick(GLfloat x, GLfloat y);
	void makeSmallBrickWell(uint x, uint y);

	bool ** get_current_mask();

	void test_print();

	void print_pos() {
		std::cout<<" x -> "<<x<<" ; "<<y<<" <- y"<<std::endl;
		std::cout<<" _glx -> "<<_glx<<" ; "<<_gly<<" <- _gly"<<std::endl;
	}
	unsigned int getx() { return x; }
	unsigned int gety() { return y; }

	unsigned int getx_shadow() { return shadow_x; }
	unsigned int gety_shadow() { return shadow_y; }

	void update_glxy();
	void update_shadow_glxy();

	void update_shadow();

	void fall();
	void shadow_fall();

	void move_left();
	void move_right();

	void create_from_mask(bool mask[16]);

};

#endif