#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include "TextPrinter.h"

class CPlayer
{
	unsigned int score;
	unsigned int level;

	unsigned int exp; // maybe used sometime
	unsigned int char_level;

	unsigned int bricks_used;
	unsigned int lines_combo;
	unsigned int lines_cleared;
	unsigned int double_lines_cleared;
	unsigned int triple_lines_cleared;
	unsigned int quad_lines_cleared;

	std::string name;

	CTextPrinter * txt;
	/*
=======

	unsigned int blocks_used;
	unsigned int lines_in_a_row;
	unsigned int lines_total;

>>>>>>> Stashed changes
*/
public:
	CPlayer(void);
	~CPlayer(void);

	void reset();
	void draw();

	void add_score(unsigned int s=1);

	void update_cleared_lines(unsigned int lines);
	void add_bricks_used() { bricks_used++; }

	void line_combo(bool new_line) {
		if (!new_line && lines_combo > 0) {
			while (lines_combo > 0) {
				score += 10*lines_combo;
				lines_combo--;
			}
		}
		(new_line) ? lines_combo++ : lines_combo = 0; 
	}

	unsigned int get_score();
	void draw_score();

	void add_level();
	unsigned int get_level();
	void draw_level();
};


#endif
