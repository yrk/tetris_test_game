#include "GameManager.h"
#include "KeyboardHandler.h"
#include <gl/gl.h>

void CGameManager::setupHandler() {
	//GLfloat x1,x2,y1,y2; // size of scene (ortho)
	//GLfloat ratio;
	//GLfloat zNear, zFar;

	//engine->init_bmp("dragon_sprite.bmp");
	userSpaceSize = CWindowGL::inst()->getUserSpaceSize();

	glViewport(0,0,userSpaceSize.x, userSpaceSize.y);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float ratio = userSpaceSize.y / (float) userSpaceSize.x;

	y1 = ratio*x1;
	y2 = ratio*x2;
	
	if (!isometricProjection)
	{
		glFrustum(-0.1, 0.1, ratio*-0.1, ratio*0.1, 0.3, 100.0);
	}
	else
	{
		//glOrtho(-3, 3, wsp*-3, wsp*3, 0.3, 100.0);
		//glOrtho(-10, 10, wsp*-10, wsp*10, 0.3, 100.0);
		glOrtho(x1, x2, y1, y2, zNear, zFar);
		engine->set_scene_size(x1,x2,y1,y2);
	}

	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_TEXTURE_2D);
	//glEnable(GL_LIGHTING);
}
	
void CGameManager::resizeHandler() {
}

void CGameManager::drawBegin() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	// for Ortho projection
	//glTranslatef(0.0, 0.0, -10.0);

	// for perspective projection
	glTranslatef(0.0, 0.0, -30.0);
	glTranslatef(0.0f, y1, 0.0f);
}

void CGameManager::drawEnd() {
	glFlush();
	SwapBuffers(CWindowGL::inst()->hdc());
}
	
void CGameManager::drawHandler() {
	this->drawBegin();

	engine->draw();

	this->drawEnd();
}


void CGameManager::doGameLoop(float seconds) {
	// check time of last frame generation
	newTime = seconds;
	deltaTime = newTime - oldTime;
	oldTime = newTime;
	
	++framesCount;
	framesPerSecondTime += deltaTime;
	gameTimeElapsed += deltaTime;

	// update keyboard
	CKeyboardHandler::inst()->update();

	this->drawBegin();

	switch(current_state) 
	{
		case GameState_Title : 
			{
				title->update();
				title->draw();
				break;
			}
		case GameState_Run : 
			/* Game Engine code */
			if (!engine->game_is_over()) {
				engine->update(deltaTime);
				engine->draw();
				//this->drawHandler();
			} else {
				current_state = GameState_End;
			}
			break;
		case GameState_End : break;
		case GameState_Menu : 
			{
				menu->update();
				menu->draw();
				break;
			}
		case GameState_HiScores : break;
		case GameState_Unknown : break;
	}
	
	this->drawEnd();



	// FPS calculations
	if (framesPerSecondTime >= 1.0f) {
		// fps value refresh
		fps = framesCount / framesPerSecondTime;
		framesCount = 0;
		framesPerSecondTime = 0.0f;
		
		// pass FPS value to engine for display in-game
		engine->set_fps((unsigned int) fps);
	}
}



CGameManager::CGameManager() : 
										current_state(GameState_Title),
										newTime(0.0f),
										oldTime(0.0f),
										deltaTime(0.0f),
										framesPerSecondTime(0.0f),
										fps(0.0f),
										framesCount(0),
										gameTimeElapsed(0.0f),
										x1(-10.0f),
										x2(10.0f),
										zNear(0.3f),
										zFar(100.0f) {

	engine = new CGameEngine();
	menu = new CMainMenu();
	title = new CTitleScreen();
}

template<class T> void CGameManager::destroy(T t) {
	if (t != NULL) {
		delete t;
		t = NULL;
	}
}

CGameManager::~CGameManager(void)
{
	destroy<CGameEngine*>(engine);
	destroy<CGameManager*>(gmInstance);
	destroy<CMainMenu*>(menu);
	destroy<CTitleScreen*>(title);
}

CGameManager* CGameManager::gmInstance = NULL;

CGameManager* CGameManager::inst() {
	if (!gmInstance) {
		gmInstance = new CGameManager;
	}

	return gmInstance;
}