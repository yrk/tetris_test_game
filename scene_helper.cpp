#include "scene_helper.h"

const GLfloat CSceneHelper::default_color[3] = {0.4f, 0.4f, 1.0f};

CSceneHelper::CSceneHelper() : sbs(0.2f) {
}

CSceneHelper::~CSceneHelper() {
}

void CSceneHelper::setBrickPos(GLfloat x, GLfloat y) {
	brick_x = x;
	brick_y = y;
}

void CSceneHelper::draw_brickL() {
	//GLfloat brick_x, brick_y;
	GLfloat brick_space = 0.0;
	GLfloat dsbs = 2*sbs; // dsbs - dobule sbs
		//brick_x = brick_y = 0.0;
	bool brickL[3][3] = {	0, 1, 0,
							0, 1, 0,
							0, 1, 1 };
	for (int j=0; j < 3; ++j) {
		for (int i=0; i < 3; ++i) {
			if (brickL[j][i]) {
				makeSmallBrick(brick_x + i*(dsbs+brick_space),brick_y - j*(dsbs+brick_space));
			}
		}
	}
}
	/*
	void draw_brickL(GLfloat x, GLfloat y) {
		glPushMatrix();
		glTranslatef(x,y,0.0);
		draw_brickL();
		glPopMatrix();
	}
	*/

void CSceneHelper::makeRect(GLfloat x, GLfloat y) {
	glPushMatrix();
	glTranslatef(x,y,0.0);
	makeRect();
	glPopMatrix();
}
void CSceneHelper::makeRect() {
	//glEdgeFlag(GL_FALSE);
	glBegin(GL_TRIANGLE_STRIP);
		
		//glColor3f(0.4,0.4,1.0);
		glColor3fv(default_color);
		
		glVertex3f(-1.0,-1.0,0.0);
		glVertex3f(1.0,-1.0,0.0);
		glVertex3f(-1.0,1.0,0.0);
		glVertex3f(1.0,1.0,0.0);
		
		glColor3f(1.0,1.0,1.0);
	glEnd();
}

void CSceneHelper::makeSmallBrick() {
		
	glBegin(GL_TRIANGLE_STRIP);
		glColor3fv(default_color);
			
		glVertex3f(-sbs,-sbs,0.0);
		glVertex3f(sbs,-sbs,0.0);
		glVertex3f(-sbs,sbs,0.0);
		glVertex3f(sbs,sbs,0.0);
		
		glColor3f(1.0,1.0,1.0);
	glEnd();

}

void CSceneHelper::makeSmallBrick(GLfloat x, GLfloat y) {
	glPushMatrix();
	glTranslatef(x,y,0.0);
	makeSmallBrick();
	glPopMatrix();
}