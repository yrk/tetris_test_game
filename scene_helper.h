#ifndef SCENE_HELPER_H
#define SCENE_HELPER_H

#include <windows.h>
#include <GL/GL.h>
/*
bool gbrickL[3][3] = { 0, 1, 0,
					  0, 1, 0,
					  0, 1, 1 };
*/
class CSceneHelper {
public:
	// CONSTS
	const static GLfloat default_color[3];
private:

	const GLfloat sbs; // sbs - small brick size
	// VARS
	bool brick_matrix[3][3]; // matrix for representing current brick
	GLfloat brick_x, brick_y;
public:
	CSceneHelper();
	~CSceneHelper();
	void setBrickPos(GLfloat x, GLfloat y) ;
	void draw_brickL() ;


	void makeRect(GLfloat x, GLfloat y) ;
	void makeRect() ;

	void makeSmallBrick();

	void makeSmallBrick(GLfloat x, GLfloat y);
};



#endif