#ifndef _GAME_MANAGER_H_
#define _GAME_MANAGER_H_

#define WIN32_LEAN_AND_MEAN

// my headers
#include "CWindowGL.h"
#include "CGameEngine.h"
#include "MainMenu.h"
#include "TitleScreen.h"

enum TGameState {
	GameState_Init,
	GameState_Title,
	GameState_Run,
	GameState_End,
	GameState_Menu,
	GameState_HiScores,
	GameState_Unknown
};

class CGameManager
{
	
public:
	// init stuff after window creation
	void setupHandler(); 
	// do things after window resize
	void resizeHandler();
	// draw all content 
	void drawHandler();
	// game loop handler
	void doGameLoop(float seconds);

	void changeGameState(TGameState newState) { current_state = newState; }

	float getDeltaTime() { return deltaTime; }
	TGameState getCurrentState() { return current_state; }

	void setHDC(HDC hdc) { handleDC = hdc; }
	void setUserSpaceSize(POINT uss) { userSpaceSize = uss; }
public:
	static CGameManager * inst();
	~CGameManager(void);

private:
	CGameManager();
	CGameManager(CGameManager const&) {}
	static CGameManager* gmInstance;

private:
	CGameEngine * engine;
	CMainMenu * menu;
	CTitleScreen * title;
	TGameState current_state;

	float newTime, oldTime, deltaTime;
	float framesPerSecondTime, fps;
	unsigned int framesCount;
	float gameTimeElapsed;

private:
	template<class T> void destroy(T t);
	void drawBegin();
	void drawEnd();

	HDC handleDC;
	POINT userSpaceSize;

	GLfloat x1,x2,y1,y2; // size of scene (ortho)
	GLfloat ratio;
	GLfloat zNear, zFar;
	bool isometricProjection;
};

#endif