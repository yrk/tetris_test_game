#ifndef _TEXT_PRINTER_H_
#define _TEXT_PRINTER_H_

#include <windows.h>

//#include <cstdio.h>
#include <string.h>
#include <iostream>



class CTextPrinter
{
	const unsigned int CHAR_LIST_SIZE;

	HFONT hFont;
	unsigned int textList;

	//HDC hDC;
//protected:
	//void createBitmapFont(char * fontName, int fontSize);
	//void clearFont(unsigned int base);
public:
	CTextPrinter();
	CTextPrinter(char * fontName, int fontSize);
	~CTextPrinter(void);

	void printString(unsigned int base, const char * str);

	void print(const char * str);

	void createBitmapFont(char * fontName, int fontSize);
	void clearFont(unsigned int base);
};

#endif