#include "TitleScreen.h"

#include "GameManager.h"
#include "KeyboardHandler.h"

#include <gl/gl.h>

CTitleScreen::CTitleScreen(void)
{
	title_txt = new CTextPrinter("Impact", 48);
	blink_txt = new CTextPrinter("Courier", 24);
	blink_color = 0.0f;
	blink_ascend = true;
	blink_delta = 0.0005f;
}


CTitleScreen::~CTitleScreen(void)
{
	delete title_txt;
	delete blink_txt;

}

void CTitleScreen::prepare_bricks() {

}

void CTitleScreen::update_blink() {
	if (blink_color < 1.0f && blink_color >0.0f) {
		if (blink_ascend) {
			blink_color += blink_delta;
		} else {
			blink_color -= blink_delta;
		}
	} else {
		blink_ascend = !blink_ascend;
		if (blink_color > 1.0f) {
			blink_color = 1.0f - blink_delta;
		} else {
			blink_color = 0.0f + blink_delta;
		}
	}

}

void CTitleScreen::draw() {
	float x,y;
	x = -3.0f;
	y = 11.0f;
	glColor4f(0.8f, 0.8f, 1.0f, 1.0f);
	glRasterPos2f(x, y);
	title_txt->print("T E T R I S");

	x = -3.0f;
	y = 5.0f;
	//glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glColor4f(blink_color, blink_color, blink_color, 1.0f);
	glRasterPos2f(x, y);
	blink_txt->print("Press SPACE to start");


}

void CTitleScreen::update() {
	update_blink();
	
	if (CKeyboardHandler::inst()->spacePressed()) {
		CGameManager::inst()->changeGameState(GameState_Menu);
	}

	if (CKeyboardHandler::inst()->enterPressed()) {
		CGameManager::inst()->changeGameState(GameState_Run);
	}
}