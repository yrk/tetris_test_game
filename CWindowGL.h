#ifndef WINDOW_GL_H
#define WINDOW_GL_H

#include "CWindow.h"


class CWindowGL : public CWindow
{
private :
	HGLRC handleRC; // RC - rendering context handle
	HDC handleDC; // DC - device context handle

	static CWindowGL * wglInstance;
private:
	CWindowGL() : CWindow(), 
				  handleRC(NULL),
				  handleDC(NULL)
	{
	}
	CWindowGL(CWindowGL const &) {};

public:
	static CWindowGL * inst();

	~CWindowGL() {}

	bool PixelFormatSetup(HDC handleDC) const;
	bool InitWGL(HWND windowHandle);
	void ExitWGL(HWND windowHandle);

	POINT getUserSpaceSize() const {
		return userSpaceSize;
	}

	LRESULT WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	WPARAM run();

	HDC hdc() { return handleDC; } 
};

#endif