#ifndef _KEYBOARD_HANDLER_H_
#define _KEYBOARD_HANDLER_H_

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

//#define _USE_DIRECT_INPUT_8_
#ifdef _USE_DIRECT_INPUT_8_
#include <dinput.h>
#endif 

enum TKeyType {
	upArrowKey = 0,
	downArrowKey,
	leftArrowKey,
	rightArrowKey,
	enterKey,
	spaceKey,
	escapeKey,
	KeyTypeSize
};

typedef struct TKeyDescStruct {
	bool fistPress;
	float delay;
} TKeyDesc;

typedef struct TDIKeyDescStruct {
	bool actOnKey;
	bool keyDown;
	float timeDown;
	float velocity;
} DIKeyDescStruct;

class CKeyboardHandler
{
public:
	
	static CKeyboardHandler * inst();	
	~CKeyboardHandler(void);
	bool update();
	// key access functions for convinence
	bool upArrowPressed();
	bool downArrowPressed();
	bool leftArrowPressed();
	bool rightArrowPressed();

	bool enterPressed();
	bool spacePressed();
	bool escapePressed();

	bool keyPressed(TKeyType keyType);
#ifdef _USE_DIRECT_INPUT_8_
	bool init();
	bool keyDown(int key) { return (_keys[key] & 0x80) ? true : false; }

	void clear_keys() { ZeroMemory(_keys, 256*sizeof(char));
						ZeroMemory(superKeys, 256*sizeof(DIKeyDescStruct)); }
#endif
private:
	CKeyboardHandler(void);
	

private:
	static CKeyboardHandler * khInstance;
	
	TKeyDesc keyTypeTab[KeyTypeSize];
	float keyboardDelay;

#ifdef _USE_DIRECT_INPUT_8_
	LPDIRECTINPUT8 directInput;
	LPDIRECTINPUTDEVICE8 diDevice;
	//int _keys[256];
	char _keys[256];
	// my super duper keys table
	
#endif 
	DIKeyDescStruct superKeys[256];
};

#endif