#include "Well.h"

#include <gl/GL.h>
#include <iostream>
#include "custom_types.h"

CWell::CWell(void) : CTetrisObject(),
					 dead_pool(NULL),
					 tall(30),
					 wide(10),
					 brick_space(0.0f)
					 
					 
{
	this->set_sbs(0.2f);

	brick = NULL;
	next_brick = NULL;

	startx = 0.0f;
	endy = 0.0f;
//	starty = endy + tall * dsbs;
	endx = startx + wide * dsbs;
	starty = endy + tall * dsbs;
	

	this->alloc_dead_pool();
	this->setup_brick_types();

}


CWell::~CWell(void)
{
	this->free_dead_pool();
	if (next_brick != NULL) {
		delete next_brick;
		next_brick = NULL;
	}
	//if (shadow_brick != NULL) {
	//	delete shadow_brick;
	//	shadow_brick = NULL;
	//}
	//if (brick_types != NULL) {
	//	this->_free_tbricks_mem();
	//}
}

bool CWell::check_collison(CBrick * brick, TOrientation orient) {
	bool ** brick_mask = brick->get_current_mask();
	unsigned int x,y;
	x = brick->getx();
	y = brick->gety();

	switch (orient) {
	case TO_DOWN : 
		{
			y++;
			break;
		}
	case TO_LEFT : 
		{
			x--;
			break;
		}
	case TO_RIGHT :
		{
			x++;
			break;
		}

	}

	for (int i =0; i<4; ++i) {
		for (int j=0; j<4; ++j) {
			if (brick_mask[i][j]) {
			//if (dead_pool[y+i][x+j] && brick_mask[i][j]) {
			if (dead_pool[y+i][x+j]) {
				return true;
			}
			}
		}
	}

	return false; // no collision
}

bool CWell::check_collison_shadow(CBrick * brick) {
	bool ** brick_mask = brick->get_current_mask();
	unsigned int x,y;
	x = brick->getx_shadow();
	y = brick->gety_shadow();

	y++;

	for (int i =0; i<4; ++i) {
		for (int j=0; j<4; ++j) {
			if (brick_mask[i][j]) {
				if (dead_pool[y+i][x+j] > 0) {
					return true;
				}
			}
		}
	}
	return false; // no collision
}

void CWell::draw_next_brick_slot() {
	// ll -left lower corner
	// rl - right lower corner
	// lu - left uper corner
	// ru - right upper corner
	GLfloat llx, lly, rux, ruy;
	llx = startx - 10*dsbs;
	lly = endy - 10*dsbs;
	rux = llx + 5*dsbs;
	ruy = lly + 5*dsbs;

	llx = 5;
	lly = starty-2;
	rux = 7;
	ruy = starty;


	glPushMatrix();
	//glTranslatef(_glx+dsbs, _gly, 0.0);
	//glBegin(GL_LINE_STRIP);
	//glPolygonMode(GL_FRONT, GL_LINE);
	glBegin(GL_LINE_STRIP);
		glVertex3f(llx,lly,0.0f);
		glVertex3f(llx,ruy, 0.0f);
		glVertex3f(rux, ruy, 0.0f);
		glVertex3f(rux, lly, 0.0f);
		glVertex3f(llx,lly,0.0f);		
	glEnd();
	glPopMatrix();
}

void CWell::draw() {
	this->draw_next_brick_slot();
	// draw Well
	glPushMatrix();
	glTranslatef(_glx+dsbs, _gly, 0.0);
	glBegin(GL_LINE_STRIP);
		glVertex3f(startx,starty,0.0f);
		glVertex3f(startx,endy, 0.0f);
		glVertex3f(endx, endy, 0.0f);
		glVertex3f(endx, starty, 0.0f);
		
	glEnd();
	glPopMatrix();

	//brick->makeSmallBrickWell(wide,tall);

	glBindTexture(GL_TEXTURE_2D, texture);

	// drow content
	for (uint i =1; i<= tall; ++i) {
		for (uint j=1; j<= wide; ++j) {
			//if (dead_pool[i][j] == 1) 
			if (dead_pool[i][j] > 0) {
				this->set_current_color(dead_pool[i][j]);
				brick->makeSmallBrickWell(j,i);
			}
		}
	}

	glBindTexture(GL_TEXTURE_2D, 0);
}

void CWell::alloc_dead_pool() {
	dead_pool = new uint*[tall+1];
	for (uint i=0; i< (tall+2); ++i) {
		dead_pool[i] = new uint[wide+2];
		for (uint j=0; j< (wide+2); ++j) {
			dead_pool[i][j] = 0;
		}
	}
	// edge of well for collision detection
	for (uint i=0; i<tall+1; ++i) {
		dead_pool[i][0] = 1;
		dead_pool[i][wide+1] = 1;
	}
	for (uint i=0; i<wide+2; ++i) {
		dead_pool[tall+1][i] = 1;
	}
}

void CWell::test_print() {
	for (uint i=0; i<(tall+2); ++i) {
		for (uint j=0; j<(wide+2); ++j) {
			std::cout<< dead_pool[i][j] << " ";
		}
		std::cout<<std::endl;
	}
}

void CWell::free_dead_pool() {
	for (uint i=0; i< (tall+2); ++i) {
		delete [] dead_pool[i];
	}
	delete [] dead_pool;
	dead_pool = NULL;
}

bool CWell::brick_can_fall(CBrick * brick) {

	return true;
}

void CWell::set_sbs(GLfloat _sbs) {
	sbs = _sbs;
	dsbs = 2*sbs;
}

int CWell::check_lines() {
	int line_fill = 0;
	int lines_removed = 0;

	for (uint i =1; i<= tall; ++i) {
		for (uint j=1; j<= wide; ++j) {
			if (dead_pool[i][j] > 0) 
				line_fill++;
		}
		//std::cout<<"line_fill = "<<line_fill<<std::endl;
		if (line_fill == wide) {
			//std::cout<<"line to be removed : "<< i << std::endl;
			this->remove_line(i);
			lines_removed++;
			//std::cout<<"lines_removed = "<<lines_removed<<std::endl;
		}
		line_fill = 0;
	}
	return lines_removed;
}

void CWell::remove_line(int line) {
	for (int i =line; i>1; --i) {
		for (uint j=0; j<= wide; ++j) {
			dead_pool[i][j] = dead_pool[i-1][j];
		}
	}
}

void CWell::setup_brick_types() {

	/*bool brick_types[TBRICKS_SIZE][3][3] = {
		{	0, 1, 0,
			0, 1, 0,
			0, 1, 1 },
		{	0, 1, 0,
			0, 1, 0,
			1, 1, 0 },
		{	0, 0, 0,
			1, 1, 1,
			0, 1, 0 },
		{	0, 0, 0,
			1, 1, 0,
			0, 1, 1 },
		{	0, 0, 0,
			0, 1, 1,
			1, 1, 0 },
		{	0, 0, 0,
			1, 1, 0,
			1, 1, 0 }
	};*/

	//this->_alloc_tbricks_mem(brick_types);
}

void CWell::_alloc_tbricks_mem(bool newLayout[TBRICKS_SIZE][4][4]) {
	
	//brick_types = new bool**[TBRICKS_SIZE];

	//for (int i=0; i<TBRICKS_SIZE; ++i) {
	//	brick_types[i] = new bool*[3];

	//	for (int j=0; j<3; ++j) {
	//		brick_types[i][j] = new bool[3];
	//		for (int k=0; k<3; ++k) {
	//			brick_types[i][j][k] = newLayout[i][j][k];
	//		}
	//	}
	//}
}

void CWell::reset() {
	for (uint i =1; i<= tall; ++i) {
		for (uint j=1; j<= wide; ++j) {
			dead_pool[i][j] = 0;
		}
	}
}

void CWell::_free_tbricks_mem() {
	
	/*for (int i=0; i<TBRICKS_SIZE; ++i) {

		for (int j=0; j<3; ++j) {
			
			delete [] brick_types[i][j];
		}
		delete [] brick_types[i];
	}
	delete [] brick_types;*/
}
/*
 bool CWell::brick_types[TBRICKS_SIZE][3][3] = {
		{	0, 1, 0,
			0, 1, 0,
			0, 1, 1 },
		{	0, 1, 0,
			0, 1, 0,
			1, 1, 0 },
		{	0, 0, 0,
			1, 1, 1,
			0, 1, 0 },
		{	0, 0, 0,
			1, 1, 0,
			0, 1, 1 },
		{	0, 0, 0,
			0, 1, 1,
			1, 1, 0 },
		{	0, 0, 0,
			1, 1, 0,
			1, 1, 0 }
};
 */
  bool CWell::brick_types[TBRICKS_SIZE][4][4] = {
		{	0, 1, 0, 0,
			0, 1, 0, 0,
			0, 1, 1, 0,
			0, 0, 0, 0 },
		{	0, 0, 1, 0,
			0, 0, 1, 0,
			0, 1, 1, 0,
			0, 0, 0, 0 },
		{	0, 0, 0, 0,
			1, 1, 1, 0,
			0, 1, 0, 0,
			0, 0, 0, 0 },
		{	0, 0, 0, 0,
			1, 1, 0, 0,
			0, 1, 1, 0,
			0, 0, 0, 0 },
		{	0, 0, 0, 0,
			0, 1, 1, 0,
			1, 1, 0, 0,
			0, 0, 0, 0 },
		{	0, 0, 0, 0,
			0, 1, 1, 0,
			0, 1, 1, 0,
			0, 0, 0, 0 },
		{   0, 1, 0, 0,
			0, 1, 0, 0,
			0, 1, 0, 0,
			0, 1, 0, 0 }
};
  
 GLfloat CWell::color_table[color_size+1][3] = {
	 { 0.0f, 0.0f, 0.0f }, // fake color
	 { 0.6f, 1.0f, 0.8f },
	 { 0.8f, 0.5f, 0.5f },
	 { 0.1f, 0.8f, 0.1f },
	 { 0.8f, 0.8f, 0.8f },
	 { 0.5f, 0.5f, 0.5f },
	 { 1.0f, 1.0f, 1.0f }
 };