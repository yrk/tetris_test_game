#ifndef WELL_H
#define WELL_H

#include "custom_types.h"
//#include <windows.h>
//#include <gl/gl.h>

#include "TetrisObject.h"
#include "Brick.h"




enum TBricks {
	L_brick = 0,
	J_brick,
	T_brick,
	Z_brick,
	S_brick,
	O_brick,
	I_brick,
	TBRICKS_SIZE
};

enum TColors {
	color_weird = 0,
	color_red,
	color_green,
	color_blue,
	color_grey,
	color_white,
	color_size
};

class CWell : public CTetrisObject
{
protected:
	const GLfloat brick_space; // space between block in brick
	GLfloat sbs; // sbs - small brick size
	GLfloat dsbs; // dsbs - dobule sbs

	

	void _alloc_tbricks_mem(bool newLayout[TBRICKS_SIZE][4][4]);
	void _free_tbricks_mem();
private:

	GLfloat startx, starty, endx, endy;
	unsigned int tall, wide; // how <tall> and <wide> well is
	// sbs - small block size 
	// dsbs - dobule sbs
	
	unsigned int ** dead_pool;
//	bool *** brick_types;

	CBrick * brick;
	CBrick * next_brick;
	//CBrick * shadow_brick;
	
	unsigned int texture;

	void setup_brick_types();

	void alloc_dead_pool();
	void free_dead_pool();
public:
	void test_print();
	static bool brick_types[TBRICKS_SIZE][4][4];

	static GLfloat color_table[color_size+1][3];

	unsigned int random_color;
public:
	CWell(void);
	~CWell(void);

	void set_sbs(GLfloat _sbs);

	void set_texture(unsigned int tex) { texture = tex; }
	// function to call from CGameEngine
	void draw();
	void draw_next_brick_slot();

	bool check_collison(CBrick * brick, TOrientation orient);
	bool check_collison_shadow(CBrick * brick);

	bool brick_can_fall(CBrick * brick);

	GLfloat get_brick_space() { return brick_space;}
	GLfloat get_sbs() {return sbs;}
	GLfloat get_dsbs() {return dsbs;}

	GLfloat get_startx() {return startx;}
	GLfloat get_starty() {return starty;}
	GLfloat get_endx() { return endx;}
	GLfloat get_endy() { return endy;}

	uint get_tall() { return tall; }
	uint get_wide() { return wide; }

	CBrick * get_next_brick() { return next_brick; }
	//CBrick * get_shadow_brick() { return shadow_brick; }

	void set_start(GLfloat x, GLfloat y) {
	}

	int check_lines();
	void remove_line(int line);

	void need_new_brick() {
		/*delete brick;
		this->add_brick();*/
	}

	CBrick * add_brick() {
		//int load_brick_type = rand() % TBRICKS_SIZE;
		//brick = new CBrick(this, brick_types[load_brick_type]);
		//std::cout<<"trying to create a brick in WELL " <<std::endl;

		// firs time load - need to allocate two bricks: current and next
		if (!next_brick) {
			next_brick = new CBrick(this);
		}
		
		brick = next_brick;
		brick->texture = texture;

		next_brick = new CBrick(this);
		next_brick->texture = texture;

		//shadow_brick = new CBrick(this);
		//shadow_brick->load_brick(brick->get_brick_type());

		random_color = (rand() % color_size) +1;

		brick->set_default_color(color_table[random_color]);
		brick->current_color=random_color;
		brick->set_xy(5,0);

		//shadow_brick->set_default_color(color_table[0]);
		//shadow_brick->set_xy(5,1);

		next_brick->set_xy(13,2);

		return brick;
	}

	void set_current_color(int _color) {
		if (_color <= color_size && _color >= 1) {
			glColor3fv(color_table[_color]);
		}
		else {
			glColor3fv(color_table[1]);
		}
	}


	void copy_brick_mask_to_position(CBrick * brick) {
		/*std::cout<<" ------- well before copy ------ \n";
		this->test_print();
		std::cout<<" =============================== \n";
		std::cout<<" ---- brick to copy ------------ \n";
		brick->test_print();
		std::cout<<" =============================== \n";*/

		bool ** brick_mask = brick->get_current_mask();
		unsigned int x,y;
		x = brick->getx();
		y = brick->gety();

		for (int i=0; i<4; ++i) {
			for (int j=0; j<4; ++j) {
				if (brick_mask[i][j]) {
					//std::cout<<" copying : dead_pool["<<y+i<<"]["<<x+j<<"] = brick_mask["<<i<<"]["<<j<<"]";
					//dead_pool[y+i][x+j] = brick_mask[i][j];
					dead_pool[y+i][x+j] = brick->current_color;
				}
			}
		}
		/*std::cout<<" -------- well after copy ------ \n";
		this->test_print();
		std::cout<<" =============================== \n";*/
//		bool ** brick_mask = brick->get_current_mask();
	}

	void delete_brick_mask_at_position(CBrick * brick) {
		bool ** brick_mask = brick->get_current_mask();
		unsigned int x,y;
		x = brick->getx();
		y = brick->gety();

		for (int i=0; i<4; ++i) {
			for (int j=0; j<4; ++j) {
				if (brick_mask[i][j]) {
					dead_pool[y+i][x+j] = 0;
				}
			}
		}
	}

	void reset();
};





#endif
