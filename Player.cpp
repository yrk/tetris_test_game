#include "Player.h"

#include <sstream>
#include <gl/GL.h>

CPlayer::CPlayer(void)
{
	reset();
	txt = new CTextPrinter("Arial", 16);
}


CPlayer::~CPlayer(void)
{
	delete txt;
}

void CPlayer::reset() {
	score = 0;
	level = 1;
	bricks_used = 0;
	lines_combo = 0;
	lines_cleared = 0;
	double_lines_cleared = 0;
	triple_lines_cleared = 0;
	quad_lines_cleared = 0;

	// for future
	exp = 0;
	char_level = 0;
}

// just to draw all player info
void CPlayer::draw() {
	std::stringstream str;
	
		//"SCORE :   " + std::to_string((_ULonglong)score);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glRasterPos2f(-9.5, 13);
	str << "SCORE : "  << score;
	txt->print(str.str().c_str());

	glRasterPos2f(-9.5, 12);
	str.str(std::string());
	str.clear();
	str << "LEVEL : " << level;
	txt->print(str.str().c_str());

	glRasterPos2f(-9.5, 11);
	str.str(std::string());
	str.clear();
	str << "COMBO : " << lines_combo;
	txt->print(str.str().c_str());

	glRasterPos2f(-9.5, 10);
	str.str(std::string());
	str.clear();
	str << "2x lines : " << double_lines_cleared;
	txt->print(str.str().c_str());

	glRasterPos2f(-9.5, 9);
	str.str(std::string());
	str.clear();
	str << "Total lines : " << lines_cleared;
	txt->print(str.str().c_str());

	glRasterPos2f(-9.5, 8);
	str.str(std::string());
	str.clear();
	str << "Bricks used : " << bricks_used;
	txt->print(str.str().c_str());
}

void CPlayer::update_cleared_lines(unsigned int lines) {
	unsigned int score_inc = 0;
	switch (lines) {
	case 1: 
		score_inc = 10;
		break;
	case 2:
		score_inc = 30;
		double_lines_cleared++;
		break;
	case 3:
		score_inc = 60;
		triple_lines_cleared++;
		break;
	case 4:
		score_inc = 100;
		quad_lines_cleared++;
		break;
	}
	lines_cleared += lines;
	score += score_inc;
}

void CPlayer::add_score(unsigned int s) {
	score += s;
}

unsigned int CPlayer::get_score() {
	return score;
}

void CPlayer::draw_score() { 
}

void CPlayer::add_level() {
	level++;
}

unsigned int CPlayer::get_level() {
	return level;
}

void CPlayer::draw_level() {
}