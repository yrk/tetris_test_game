#include "EngineObject.h"


CEngineObject::CEngineObject(void) : _glx(0.0f), 
									 _gly(0.0f)
{
	set_default_color(0.4f, 0.4f, 1.0f);
}


CEngineObject::~CEngineObject(void)
{
}

void CEngineObject::set_position(GLfloat _x, GLfloat _y) {
	_glx = _x;
	_gly = _y;
}

GLfloat CEngineObject::x() {
	return _glx;
}

GLfloat CEngineObject::y() {
	return _gly;
}

void CEngineObject::set_default_color(GLfloat _r, 
									  GLfloat _g, 
									  GLfloat _b) 
{
	default_color[0] = _r;
	default_color[1] = _g;
	default_color[2] = _b;
}

void CEngineObject::set_default_color(GLfloat col[3]) {
	default_color[0] = col[0];
	default_color[1] = col[1];
	default_color[2] = col[2];

}

//void CEngineObject::draw() {
//
//}