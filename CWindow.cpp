﻿#include "CWindow.h"
#include <iostream>

#define WINDOW_BAR_NAME "Simple Tetris by yrk @ 10.2013 ver. 0.8 M5"

LRESULT CWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch (message) 
	{
	case WM_SIZE:
		RECT rect;
		GetClientRect(hWnd, &rect);
		userSpaceSize.x = rect.right - rect.left;
		userSpaceSize.y = rect.bottom - rect.top;
		break;

	case WM_KEYDOWN:
		switch(wParam)
		{
			/*
			case VK_ESCAPE:
				SendMessage(hWnd, WM_DESTROY, 0, 0);
				break;
				*/
			//case VK_UP:
			//	break;

		}
		break;
		
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default: // automatyczne przetwarzanie komunikatow
		return (DefWindowProc(hWnd, message, wParam, lParam));
	}

	return 0L;
}

bool CWindow::Init(HINSTANCE appHandle, POINT windowPosition, POINT windowSize)
{
	char windowName[] = WINDOW_BAR_NAME;

	_hInstance = appHandle;

	WNDCLASSEX wc;
	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)::WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = appHandle;
	wc.hIcon = NULL;
	wc.hIconSm = NULL;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = windowName;

	if (RegisterClassEx(&wc)==0) return false;

	bool fullscreenMode = false;

	DWORD windowStyle = WS_OVERLAPPEDWINDOW;

	if (fullscreenMode) {
		windowPosition.x = 0; 
		windowPosition.y = 0;
		windowSize.x = 1024; 
		windowSize.y = 768;
		windowStyle = WS_POPUP;
		if (!ChangeDisplayResolution(windowSize.x, windowSize.y, CDS_FULLSCREEN)) return false;
	}

	HWND windowHandle = CreateWindow(
		windowName,
		windowName,
		windowStyle,
		windowPosition.x, windowPosition.y,
		windowSize.x, windowSize.y,
		NULL,
		NULL,
		appHandle,
		NULL);

	_hWnd = windowHandle;

	if (windowHandle == NULL) return false;

	ShowWindow(windowHandle, SW_SHOW);
	UpdateWindow(windowHandle);

	return true;
}

bool CWindow::ChangeDisplayResolution(long width, long height, long colorDepth)
{
	DEVMODE dmScreenSettings;
	memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
	dmScreenSettings.dmSize = sizeof(dmScreenSettings);
	dmScreenSettings.dmPelsWidth = width;
	dmScreenSettings.dmPelsHeight = height;
	dmScreenSettings.dmBitsPerPel = colorDepth;
	dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	return ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) == DISP_CHANGE_SUCCESSFUL;
}

RECT CWindow::getDesktopSize()
{
	RECT displaySize;
	GetWindowRect(GetDesktopWindow(), &displaySize);
	//
	userSpaceSize.x = displaySize.right - displaySize.left;
	userSpaceSize.y = displaySize.bottom - displaySize.top;
	return displaySize;
}

float CWindow::getSeconds()
{
	if (!bUseQPC)   return GetTickCount() / 1000.0f;
    else {
		
		QueryPerformanceCounter (&uTicks);
		return (float)(uTicks.QuadPart / (double)uFreq.QuadPart);
	}
}



WPARAM CWindow::run()
{
	MSG msg;

	//registerEngine(new CGameEngine());
	//float fTime, fNewTime, dt;
	/*float fFPS, fFPSTime;
	unsigned uFrames;

	fTime = GetSeconds();
	fFPSTime= 0.0f;
	uFrames = 0;*/

	appIsRunning = false;

	while (appIsRunning)
	{
		if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				appIsRunning = false;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else /* Game Engine code */
		{
			/*
			// check time of last frame generation
			fNewTime = GetSeconds();
			dt = fNewTime - fTime;
			fTime = fNewTime;

			//engine->handle_input();

			      // ewentualna aktualizacja wartości FPS
			++uFrames;
			fFPSTime += dt;
			time_app_is_running += dt;

			//engine->update(dt);

			if (fFPSTime >= 1.0f)   // czy minęła sekunda?
			{
				// tak - aktualizacja FPS i zaczynamy liczyć klatki od początku
				fFPS = uFrames / fFPSTime;
				uFrames = 0;
				fFPSTime = 0.0f;
				std::cout<<"FPS : "<<fFPS << std::endl;
				std::cout<<"app is running for: "<< time_app_is_running<< std::endl;
			}

			*/
			

			
		}
	}
	return msg.wParam;

}

WPARAM CWindow::RunEvents()
{
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}