﻿#include "CWindowGL.h"
#include "GameManager.h"

CWindowGL * CWindowGL::wglInstance = NULL;

CWindowGL * CWindowGL::inst(){
	if (!wglInstance) {
		wglInstance = new CWindowGL();
	}

	return wglInstance;
}

bool CWindowGL::PixelFormatSetup(HDC handleDC) const
{
	PIXELFORMATDESCRIPTOR pixelFormatDesc;
	ZeroMemory(&pixelFormatDesc, sizeof(pixelFormatDesc));
	
	pixelFormatDesc.nVersion = 1;
	pixelFormatDesc.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
	pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
	pixelFormatDesc.cColorBits = 32;
	pixelFormatDesc.cDepthBits = 16;
	pixelFormatDesc.iLayerType = PFD_MAIN_PLANE;
	int pixelFormat = ChoosePixelFormat(handleDC, &pixelFormatDesc);
	if (pixelFormat == 0) return false;
	if (!SetPixelFormat(handleDC, pixelFormat, &pixelFormatDesc)) return false;

	return true;
}

bool CWindowGL::InitWGL(HWND windowHandle)
{
	handleDC = ::GetDC(windowHandle);
	if (!PixelFormatSetup(handleDC)) return false;
	handleRC = wglCreateContext(handleDC);
	if (handleRC == NULL) return false;
	if (!wglMakeCurrent(handleDC, handleRC)) return false;

	return true;
}

void CWindowGL::ExitWGL(HWND windowHandle)
{
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(handleRC);
	::ReleaseDC(windowHandle, handleDC);
}

LRESULT CWindowGL::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	long result = CWindow::WndProc(hWnd, message, wParam, lParam);

	//bool isometricProjection = false; //true;

	switch (message)
	{
	case WM_CREATE:
		InitWGL(hWnd);
		//SetupScene(isometricProjection);
		//CGameManager::inst()->setHDC(handleDC);
		//CGameManager::inst()->setUserSpaceSize(userSpaceSize);
		CGameManager::inst()->setupHandler();
		break;

	case WM_SIZE:
		//SetupScene(isometricProjection);
		CGameManager::inst()->setupHandler();
		break;

	case WM_PAINT:
		//DrawScene();
		//CGameManager::inst()->drawHandler();
		ValidateRect(hWnd, NULL);
		break;
/*
	case WM_CHAR:
		break;
	case WM_KEYDOWN:
		break;
	case WM_KEYUP:
		break;

	case WM_MOUSEMOVE:
		break;
	case WM_MOUSEWHEEL:
		break;

	case WM_MBUTTONDOWN:
		break;
	case WM_MBUTTONUP:
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_LBUTTONUP:
		break;
	case WM_RBUTTONDOWN:
		break;
	case WM_RBUTTONUP:
		break;
	case WM_LBUTTONDBLCLK:
		break;
	case WM_RBUTTONDBLCLK:
		break;

*/
	case WM_DESTROY:
		ExitWGL(hWnd);
		break;
	}
	return result;
}
/*
void CWindowGL::SetupScene(bool isometricProjection)
{
	engine = new CGameEngine(handleDC);
	//engine->init_bmp("dragon_sprite.bmp");

	glViewport(0,0,userSpaceSize.x, userSpaceSize.y);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float ratio = userSpaceSize.y / (float) userSpaceSize.x;

	y1 = ratio*x1;
	y2 = ratio*x2;
	
	if (!isometricProjection)
	{
		glFrustum(-0.1, 0.1, ratio*-0.1, ratio*0.1, 0.3, 100.0);
	}
	else
	{
		//glOrtho(-3, 3, wsp*-3, wsp*3, 0.3, 100.0);
		//glOrtho(-10, 10, wsp*-10, wsp*10, 0.3, 100.0);
		glOrtho(x1, x2, y1, y2, zNear, zFar);
		engine->set_scene_size(x1,x2,y1,y2);
	}

	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_TEXTURE_2D);
	//glEnable(GL_LIGHTING);
}
*/
/*
void CWindowGL::DrawScene()
{
	//const float x0 = 1.0;
	//const float y0 = 1.0;
	//const float z0 = 1.0;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	// for Ortho projection
	//glTranslatef(0.0, 0.0, -10.0);

	// for perspective projection
	glTranslatef(0.0, 0.0, -30.0);

	glTranslatef(0.0f, y1, 0.0f);

	engine->draw();

	glFlush();
	SwapBuffers(handleDC);
}
*/

WPARAM CWindowGL::run()
{
MSG msg;
/*
	//registerEngine(new CGameEngine());
	float fTime, fNewTime, dt;
	float fFPS, fFPSTime;
	unsigned uFrames;

	fTime = GetSeconds();
	fFPSTime= 0.0f;
	uFrames = 0;
	//fFPS = 0;
	*/
	while (appIsRunning)
	{
		if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				appIsRunning = false;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			CGameManager::inst()->doGameLoop(this->getSeconds());
		}

		if (CGameManager::inst()->getCurrentState() == GameState_End) {
			appIsRunning = false;
		}
			/*
			if (!engine->game_is_over())
		{
			// check time of last frame generation
			fNewTime = GetSeconds();
			dt = fNewTime - fTime;
			fTime = fNewTime;

			//engine->handle_input();

			// ewentualna aktualizacja wartości FPS
			++uFrames;
			fFPSTime += dt;
			time_app_is_running += dt;

			engine->update(dt);
			DrawScene();
			

			if (fFPSTime >= 1.0f)   // czy minęła sekunda?
			{
				// tak - aktualizacja FPS i zaczynamy liczyć klatki od początku
				fFPS = uFrames / fFPSTime;
				uFrames = 0;
				fFPSTime = 0.0f;
				//std::cout<<"FPS : "<<fFPS << std::endl;
				//std::cout<<"app is running for: "<< time_app_is_running<< std::endl;
				engine->set_fps((unsigned int) fFPS);
			}
			
			
		}
		*/
	}
	return msg.wParam;

}