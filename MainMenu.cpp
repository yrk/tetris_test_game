#include "MainMenu.h"
#include "GameManager.h"
#include "KeyboardHandler.h"

#include <gl/gl.h>

CMainMenu::CMainMenu() : current_value(Menu_New_Game),
	current_position(1)
{
	//txt_info = new CTextPrinter("Arial", 20);
	txt_info = new CTextPrinter("Consolas", 24);
	this->setup();
}


CMainMenu::~CMainMenu(void) {
	delete txt_info;
}

void CMainMenu::draw() {
	float x,y;
	std::string str;
	x = -6.0f;
	y = 11.0f;
	//for (unsigned int i = 0; i< Menu_Size; i++) {
	for (unsigned int i = 1; i<=6 ; i++) {
		if (current_position == i) {
			//x = -3.0f;
			glColor4f(0.9f, 0.9f, 0.0f, 1.0f);
		} else {
			//x = -6.0f;
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		}
		glRasterPos2f(x, y);
		//str = menu[i].name + " - "+std::to_string((_ULonglong)i);
		//txt_info->print(str.c_str());
		txt_info->print(menu[i].name.c_str());
		y -= 1.5f;
	}

	/*
	x = 5.0f;
	y = 11.0f;
	glRasterPos2f(x, y);
	str = "current_position = " + std::to_string((_ULonglong) current_position);
	txt_info->print(str.c_str());
	*/
}

void CMainMenu::update() {
	if (CKeyboardHandler::inst()->upArrowPressed()) {
		//if (current_position == Menu_Continue_Game) {
			//current_position = Menu_Exit;
			//current_position = Menu_Continue_Game;
		if (current_position > 1) {
			--current_position;
		} else {
			current_position = 6;
		}
	}
	if (CKeyboardHandler::inst()->downArrowPressed()) {
		//if (current_position == Menu_Exit) {
			//current_position = Menu_Exit;
			//current_position = Menu_Continue_Game;
		if (current_position < 6) {
			++current_position;
		} else {
			current_position = 1;
		}
	}
	if (CKeyboardHandler::inst()->enterPressed()) {
		switch (current_position) {
		case Menu_New_Game : {
			CGameManager::inst()->changeGameState(GameState_Run);
			break;
							 }
		case Menu_Exit : {
			CGameManager::inst()->changeGameState(GameState_End);
			break;
						 }
		}
	}
}

MenuValues CMainMenu::get_value() {
	return current_value;
}

void CMainMenu::setup() {
	//for (unsigned int i = 0; i<Menu_Size; ++i) {
//		menu[i].value = i;
	//	menu[i].position = i;
	//}
	menu[0].name = "FAKE";
	menu[1].name = "Continue Game";
	menu[2].name = "New Game";
	menu[3].name = "Load Game";
	menu[4].name = "Save Game";
	menu[5].name = "High Scores";
	menu[6].name = "Exit";

	/*
	menu[0].name = "FAKE";
	menu[Menu_Continue_Game].name = "Continue Game";
	menu[Menu_New_Game].name = "New Game";
	menu[Menu_Load_Game].name = "Load Game";
	menu[Menu_Save_Game].name = "Save Game";
	menu[Menu_HiScores].name = "High Scores";
	menu[Menu_Exit].name = "Exit";
	*/
	//current_value = Menu_New_Game;
	//current_position = 1;
}

void CMainMenu::cursor_down() {
}

void CMainMenu::cursor_up() {
}

void CMainMenu::setTextPrinter(CTextPrinter * txt) {
	if (txt_info != NULL) {
		delete txt_info;
		txt_info = NULL;
	}
	txt_info = txt;
}