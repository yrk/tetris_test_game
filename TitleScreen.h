#ifndef _TITLE_SCREEN_H_
#define _TITLE_SCREEN_H_

#include "TextPrinter.h"

class CTitleScreen
{
public:
	CTitleScreen(void);
	~CTitleScreen(void);

	// make all bricks needed for title screen
	void prepare_bricks();
	// draw tittle screen
	// TETRIS <- with brick
	// PRESS SPACE TO START <- blinking !
	void draw();
	void update();
private :
	CTextPrinter * title_txt;
	CTextPrinter * blink_txt;

	float blink_color;
	float blink_delta;
	bool blink_ascend;

	void update_blink();
};

#endif