#ifndef DND_CONTROLLER_H
#define DND_CONTROLLER_H

enum BUTTON = {MOUSE_LEFT_CLICK, MOUSE_RIGHT_CLICK, MOUSE_MIDDLE_CLICK };

class CAnyObjectType {
	int xmouse, ymouse;
	POINT mouse_pos;
	POINT last_mouse_pos;

public:
	virtual POINT getMousePoint() { return mouse_pos; }
	virtual POINT getLastMousePoint() { return last_mouse_pos; }
	virtual void mouseClicked(int x, int y, BUTTON button);
	virtual void mouseReleased(int x, int y, BUTTON button);
	virtual void mouseMoved() {}


};

class DNDController : CAnyObjectType {


};


#endif