#include "Brick.h"
#include "Well.h"

CBrick::CBrick(CWell * my_well) : 
	current_orient(TO_NORMAL),
	layouts(NULL), 
	well(my_well),
	current_color(0),
	_close_to_left_wall(false),
	_close_to_right_wall(false)
{
	x = 0;
	y = 3;

	shadow_x = x;
	shadow_y = y;

	glx = gly = 0.0;
	glx = well->get_startx();
	gly = well->get_starty();

	sbs = well->get_sbs();
	dsbs = well->get_dsbs();
	brick_space = well->get_brick_space();

	this->load_brick();

	current_mask = layouts[current_orient];
}

CBrick::~CBrick(void)
{
	if (layouts != NULL) {
		this->_free_layout_mem();
	}
}

//void CBrick::load_layouts(bool newLayout[4][3][3]) {
void CBrick::load_layouts(bool newLayout[4][4][4]) {
	this->_alloc_layout_mem_set(newLayout);
}

void CBrick::close_to_wall_update() {
	//if (x >= 0 && x <= 2) {
	if ( x < 2) {
		_close_to_left_wall = true;
	} 
	else {
		_close_to_left_wall = false;
	}
	
	if (x >= well->get_wide()-3) { // minus 2, but starting from 0
		_close_to_right_wall = true;
	} 
	else {
		_close_to_right_wall = false;
	}
}

void CBrick::set_xy(unsigned int _x, unsigned int _y) {
	x = _x;
	y = _y;
//	glx = well->get_startx() + x*(dsbs+brick_space);
//	gly = well->get_starty() + y*(dsbs+brick_space);
	shadow_x = x;
	shadow_y = y;

	//this->update_shadow_glxy();
	this->update_glxy();
	this->update_shadow_glxy();
}

void CBrick::update_glxy() {
	set_position(well->get_startx() + x*(dsbs+brick_space),
				 well->get_starty() - y*(dsbs+brick_space));

	//shadow_x = x;
	//shadow_y = y;
	////this->update_shadow_glxy();

	//while (!well->check_collison_shadow(this) ) {
	//		this->shadow_fall();
	//}
	//this->update_shadow_glxy();

}

void CBrick::update_shadow_glxy() {
	shadow_glx = well->get_startx() + shadow_x*(dsbs+brick_space);
	shadow_gly = well->get_starty() - shadow_y*(dsbs+brick_space);
}

void CBrick::draw_simple() {
	glColor3fv(default_color);
	glBindTexture(GL_TEXTURE_2D, texture);

	for (int j=0; j < 4; ++j) {
		for (int i=0; i < 4; ++i) {
			if (layouts[current_orient][j][i]) {
				this->makeSmallBrick(_glx + i*(dsbs+brick_space),_gly - j*(dsbs+brick_space));
			}
		}
	}
}

void CBrick::draw() {

	glColor3fv(default_color);
	glBindTexture(GL_TEXTURE_2D, texture);

	for (int j=0; j < 4; ++j) {
		for (int i=0; i < 4; ++i) {
			if (layouts[current_orient][j][i]) {
				this->makeSmallBrick(_glx + i*(dsbs+brick_space),_gly - j*(dsbs+brick_space));
			}
		}
	}
	/*
	// draw brick shadow - to see where brick fits
	GLfloat LightAmbient[]=  { 0.5f, 0.5f, 0.5f, 1.0f };    // Ambient Light Values
	GLfloat LightDiffuse[]=  { 1.0f, 1.0f, 1.0f, 1.0f };    // Diffuse Light Values
	GLfloat LightPosition[]= { 0.0f, 0.0f, 2.0f, -100.0f };    // Light Position
	*/
	//glColor4f(1.0f, 1.0f, 1.0f,0.4f);
	glColor4f(default_color[0],default_color[1],default_color[2],0.6f);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);

	glEnable(GL_BLEND);     // Turn Blending On
    glDisable(GL_DEPTH_TEST);

	for (int j=0; j < 4; ++j) {
		for (int i=0; i < 4; ++i) {
			if (layouts[current_orient][j][i]) {
				this->makeSmallBrick(shadow_glx + i*(dsbs+brick_space),shadow_gly - j*(dsbs+brick_space));
			}
		}
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glColor3f(1.0f, 1.0f, 1.0f);

    glDisable(GL_BLEND);        // Turn Blending Off
    glEnable(GL_DEPTH_TEST);    // Turn Depth Testing On

}

void CBrick::rotate(TOrientation orient) {
	int i=0;
	TOrientation tab[4] = {TO_NORMAL,
		TO_RIGHT,
		TO_DOWN,
		TO_LEFT };
	while (tab[i] != current_orient) i++;

	TOrientation next_orient;

	if (tab[i] != TO_LEFT) 
		next_orient = tab[i+1];
	else next_orient = tab[0];

	current_mask = layouts[next_orient];

	// remember previous x and y value :
	unsigned int prev_x = x;
	unsigned int prev_y = y;

	if (!well->check_collison(this, TO_NORMAL)){
		current_orient = next_orient;
	} else {
		// try to move brick off the wall
		if (close_to_left_wall()) {
			while (close_to_left_wall() && well->check_collison(this, TO_NORMAL) ){
				x++;
			}
			if (!well->check_collison(this, TO_NORMAL)){
				current_orient = next_orient;
			} else {
				x = prev_x;
			}
		}

		if (close_to_right_wall()) {
			while (close_to_right_wall() && well->check_collison(this, TO_NORMAL)){
				x--;
			}
			if (!well->check_collison(this, TO_NORMAL)){
				current_orient = next_orient;
			} else {
				x = prev_x;
			}
		}
		current_mask = layouts[current_orient];
	}

	shadow_x = x;
	shadow_y = y;

	this->close_to_wall_update();
	this->update_glxy();
	this->update_shadow();
}

void CBrick::move(TOrientation dest) {
}

bool ** CBrick::get_current_mask() {
	return current_mask;
}

void CBrick::rotate_matrix33(bool src_mat[3][3], bool dest_mat[3][3]) {
	dest_mat[0][0] = src_mat[2][0];
	dest_mat[0][1] = src_mat[1][0];
	dest_mat[0][2] = src_mat[0][0];
	dest_mat[1][0] = src_mat[2][1];

	dest_mat[1][1] = src_mat[1][1];

	dest_mat[1][2] = src_mat[0][1];
	dest_mat[2][0] = src_mat[2][2];
	dest_mat[2][1] = src_mat[1][2];
	dest_mat[2][2] = src_mat[0][2];
}

void CBrick::rotate_matrix44(bool src_mat[4][4], bool dest_mat[4][4]) {
	dest_mat[0][0] = src_mat[3][0];
	dest_mat[0][1] = src_mat[2][0];
	dest_mat[0][2] = src_mat[1][0];
	dest_mat[0][3] = src_mat[0][0];

	dest_mat[1][0] = src_mat[3][1];
	dest_mat[1][1] = src_mat[2][1];
	dest_mat[1][2] = src_mat[1][1];
	dest_mat[1][3] = src_mat[0][1];


	dest_mat[2][0] = src_mat[3][2];
	dest_mat[2][1] = src_mat[2][2];
	dest_mat[2][2] = src_mat[1][2];
	dest_mat[2][3] = src_mat[0][2];

	dest_mat[3][0] = src_mat[3][3];
	dest_mat[3][1] = src_mat[2][3];
	dest_mat[3][2] = src_mat[1][3];
	dest_mat[3][3] = src_mat[0][3];
	// push brick in matrix to the most left - fix 4L brick issue
	if (dest_mat[0][0] == 0 &&
		dest_mat[1][0] == 0 &&
		dest_mat[2][0] == 0 &&
		dest_mat[3][0] == 0 ) {
			for (unsigned int i = 0; i<3; ++i) {
				dest_mat[0][0+i] = dest_mat[0][1+i];
				dest_mat[1][0+i] = dest_mat[1][1+i];
				dest_mat[2][0+i] = dest_mat[2][1+i];
				dest_mat[3][0+i] = dest_mat[3][1+i];
			}
			for (unsigned int i = 0; i<3; ++i) {
				dest_mat[0][3] = 0;
				dest_mat[1][3] = 0;
				dest_mat[2][3] = 0;
				dest_mat[3][3] = 0;
			}
	}
}

void CBrick::create_rotation_matrix33(bool ** new_brick) {
	bool brick_mask[4][3][3] = {{	0, 1, 0,
									0, 1, 0,
									0, 1, 1 },
	{ 0,0,0,
	  1,1,1,
	  1,0,0},
	{ 1,1,0,
	  0,1,0,
	  0,1,0},
	{ 0,0,1,
	  1,1,1,
	  0,0,0}
	};
}

//void CBrick::load_brick(bool brick_type[3][3]) {
void CBrick::load_brick(bool brick_type[4][4]) {
	//bool brick_mask[4][3][3];
	bool brick_mask[4][4][4];
	memset(brick_mask, 0, sizeof(brick_mask));
/*	
	this->rotate_matrix33(brick_type, brick_mask[0]);
	this->rotate_matrix33(brick_mask[0], brick_mask[1]);
	this->rotate_matrix33(brick_mask[1], brick_mask[2]);
	this->rotate_matrix33(brick_mask[2], brick_mask[3]);
*/
	this->rotate_matrix44(brick_type, brick_mask[0]);
	this->rotate_matrix44(brick_mask[0], brick_mask[1]);
	this->rotate_matrix44(brick_mask[1], brick_mask[2]);
	this->rotate_matrix44(brick_mask[2], brick_mask[3]);

	this->load_layouts(brick_mask);
}



void CBrick::load_brick()
{

	int rand_type = rand() % TBRICKS_SIZE;
	if (rand_type >6) rand_type = 6;

	brick_type = rand_type;

	this->load_brick(rand_type);
	/*
	bool brick_mask[4][4][4];
	memset(brick_mask, 0, sizeof(brick_mask));
	
	this->rotate_matrix44(well->brick_types[rand_type], brick_mask[0]);
	this->rotate_matrix44(brick_mask[0], brick_mask[1]);
	this->rotate_matrix44(brick_mask[1], brick_mask[2]);
	this->rotate_matrix44(brick_mask[2], brick_mask[3]);

	this->load_layouts(brick_mask);
	*/
}

void CBrick::load_brick(int rand_type)
{
	if (rand_type >6) rand_type = 6;

	brick_type = rand_type;

	bool brick_mask[4][4][4];
	memset(brick_mask, 0, sizeof(brick_mask));
	
	this->rotate_matrix44(well->brick_types[rand_type], brick_mask[0]);
	this->rotate_matrix44(brick_mask[0], brick_mask[1]);
	this->rotate_matrix44(brick_mask[1], brick_mask[2]);
	this->rotate_matrix44(brick_mask[2], brick_mask[3]);

	this->load_layouts(brick_mask);
}

void CBrick::_alloc_layout_mem() {
	
	layouts = new bool**[4];

	for (int i=0; i<4; ++i) {
		layouts[i] = new bool*[3];

		for (int j=0; j<3; ++j) {
			layouts[i][j] = new bool[3];
		}
	}
}

//void CBrick::_alloc_layout_mem_set(bool newLayout[4][3][3]) {
void CBrick::_alloc_layout_mem_set(bool newLayout[4][4][4]) {
	
	layouts = new bool**[4];

	for (int i=0; i<4; ++i) {
		//layouts[i] = new bool*[3];
		layouts[i] = new bool*[4];

		for (int j=0; j<4; ++j) {
			//layouts[i][j] = new bool[3];
			layouts[i][j] = new bool[4];
			for (int k=0; k<4; ++k) {
				layouts[i][j][k] = newLayout[i][j][k];
				//std::cout<<"layouts["<<i<<"]["<<j<<"]["<<k<<"] = "<< newLayout[i][j][k] << std::endl;
			}
		}
	}
}

void CBrick::_free_layout_mem() {
	
	for (int i=0; i<4; ++i) {

		//for (int j=0; j<3; ++j) {
		for (int j=0; j<4; ++j) {
			
			delete [] layouts[i][j];
		}
		delete [] layouts[i];
	}
	delete [] layouts;
}

void CBrick::makeSmallBrick() {
	//glBegin(GL_TRIANGLE_STRIP);
	glBegin(GL_QUADS);
		//glColor3fv(default_color);

		//glVertex3f(0.0f,0.0f,0.0);
		//glVertex3f(dsbs,0.0f,0.0);
		//glVertex3f(0.0f,dsbs,0.0);
		//glVertex3f(dsbs,dsbs,0.0);
		
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(0.0f,0.0f,0.0);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0.0f,dsbs,0.0);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(dsbs,dsbs,0.0);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(dsbs,0.0f,0.0);
		
		//glColor3f(1.0,1.0,1.0);
	glEnd();

}

void CBrick::makeSmallBrick(GLfloat x, GLfloat y) {
	glPushMatrix();
	glTranslatef(x,y,0.0);
	makeSmallBrick();
	glPopMatrix();
}

void CBrick::makeSmallBrickWell(uint x, uint y) {
	GLfloat a = well->get_startx() + x*(dsbs+brick_space);
	GLfloat b = well->get_starty() - y*(dsbs+brick_space);
	glPushMatrix();
	glTranslatef(a,b,0.0);
	makeSmallBrick();
	glPopMatrix();
}

void CBrick::test_print() {
	for (int i=0; i<3; ++i) {
		for (int j=0; j<3; ++j) {
			std::cout<<current_mask[i][j]<<" ";
		}
		std::cout<<std::endl;
	}
}

void CBrick::update_shadow() {
		shadow_y = y;
	//this->update_shadow_glxy();

	while (!well->check_collison_shadow(this) ) {
			this->shadow_fall();
	}
	this->update_shadow_glxy();
}

void CBrick::shadow_fall() {
	if (shadow_y < well->get_tall() ) {
		shadow_y++;
		//if (well->check_collison(this)) {
		//	// falling stoped, we have collision
		//	--y; // return to previous value
		//	// we need new brick
		//} else {
		//	well->copy_brick_mask_to_position(last_brick);

		//}
	}
	this->update_shadow_glxy();
}

void CBrick::fall() {
	if (y < well->get_tall() ) {
		y++;
	}
	this->update_glxy();
	this->update_shadow();
}

void CBrick::move_left() {

	if (x > 0 && !well->check_collison(this, TO_LEFT) ) {
	//if (!well->check_collison(this, TO_LEFT) ) {
		--x;
		--shadow_x;
	}
	this->close_to_wall_update();
	this->update_glxy();
	this->update_shadow();
}

void CBrick::move_right() {
	if (x < well->get_wide() && !well->check_collison(this, TO_RIGHT) ) {
		++x;
		++shadow_x;
	}
	this->close_to_wall_update();
	this->update_glxy();
	this->update_shadow();
}

void CBrick::create_from_mask(bool mask[16]) {
	static unsigned int brick_nr = 0;

//	new_bricks[brick_nr] = &mask[0];
}