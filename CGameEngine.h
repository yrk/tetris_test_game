#ifndef GAME_ENGINE_H
#define GAME_ENGINE_H

//#include <Winuser.h>
#include <iostream>
#include <string>
#include <Windows.h>

#include <gl/GLU.h>

#include <list>

#include "EngineObject.h"
#include "Brick.h"
#include "Well.h"
#include "TextPrinter.h"

#include "Player.h"

#include "MainMenu.h"

#define keypressed(x) GetAsyncKeyState(x)

#define X_POSITION_CHANGE 0.4f
#define Y_POSITION_CHANGE 0.4f

#define BITMAP_ID 0x4D42

typedef std::list<CEngineObject*> obj_list_t;
typedef std::list<CEngineObject*>::iterator obj_list_it;

/*
typedef struct tagBITMAPFILEHEADER {
	WORD bfType;
	DWORD bfSize;
	WORD bfReserved1;
	WORD bfReserved2;
	DWORD bfOffBits;
} BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER {
	DWORD biSize;
	LONG biWidth;
	LONG biHeight;
	WORD biPlanes;
	WORD biBitCount;
	DWORD biCompression;
	DWORD biSizeImage;
	LONG biXPelsPerMeter;
	LONG biYPelsPerMeter;
	DWORD biClrUsed;
	DWORD biClrImportant;
} BITMAPINFOHEADER;
*/
class CGameEngine {
	CPlayer player;
	//float ypos, xpos;
	//float x_pos_delta, y_pos_delta;
	float time_elapsed;
	float fall_time; // how many time passes before fall
	float fall_delay; // how many time is needed to fall one line
	int char_per_sec;
	float time_for_char;

	bool game_over;
	bool game_pause;
	bool game_started;

	CBrick * last_brick, *next_brick;
	CWell * well;

	obj_list_t objects;

	GLUquadricObj *quadric; ///< - wyorzystywane do rysowania strzalek
	GLfloat x1,x2,y1,y2;

	CTextPrinter * txt, *txt2;
	CTextPrinter * speed_txt;

	unsigned int fall_speed;

	BITMAPINFOHEADER bmp_info;
	unsigned char * bmp_data;

	//GLuint texture[3];
	GLuint texture;
	//unsigned int texture;
	unsigned int FPS;

	float brick3d_angle;
	float string_blink_color;

public:
	CGameEngine() : //ypos(0.0f), xpos(0.0f),
					//x_pos_delta(X_POSITION_CHANGE),
					//y_pos_delta(Y_POSITION_CHANGE),
					time_elapsed(0.0f),
					fall_time(0.0f),
					fall_delay(0.3f),
					char_per_sec(20),
					game_over(false),
					game_pause(false),
					FPS(0),
					brick3d_angle(0.0f),
					string_blink_color(0.0f)

					
	{
		time_for_char = 1.0f / (float) char_per_sec;
		//brick = new CBrick();

		//well = new CWell();
		this->setup();
		txt = new CTextPrinter("Arial", 16);
		txt2 = new CTextPrinter("Courier", 34);
		speed_txt = new CTextPrinter("Arial", 20);
		
	}
	~CGameEngine() {
		//if (brick != NULL) {
		//	delete brick;
		//}
		//if (well != NULL) {
		//	delete well;
		//}
		this->cleanup();
		if (txt != NULL) {
			delete txt;
		}
		if (txt2 != NULL) {
			delete txt2;
		}
		if (speed_txt != NULL) {
			delete speed_txt;
		}
	}
	void set_fps(unsigned int _fps) { FPS = _fps; }
	void print_fps();
	bool game_is_over() {return game_over; }

	void reset_shadow_brick();

	// to be deleted
	CBrick * getBrick() { return last_brick; }
	void tryBrickFall();

	void handle_input(); // get keys status and make some actions

	// reset well, bricks and player data
	void reset() { /* TO BE DONE */}

	// setup all things, reserve memory, reset to defaults
	void setup(); 
	void cleanup();

	void set_scene_size(GLfloat _x1,
						GLfloat _x2,
						GLfloat _y1,
						GLfloat _y2) {
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
	}

	void update(float dt);
	
	void add_brick();

	void print(void* font, char* s);

	void draw();
	void draw_axis();

	void init_brick(unsigned int aspect, CBrick * brick);

	unsigned char * LoadBitmapFile(char * filename, BITMAPINFOHEADER *bitmapInfoHeader);

	void init_bmp(char * bmp_name);

	void draw_bitmap();

	void loadGLTextures();

	void draw_brick3d();
};

#endif
