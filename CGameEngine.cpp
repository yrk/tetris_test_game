#include "CGameEngine.h"
#include "KeyboardHandler.h"
#include "GameManager.h"

void CGameEngine::update(float dt)
{
	if (game_over || game_pause) {
		handle_input();
		return;
	}
	time_elapsed += dt;
	fall_time += dt;
	// input speed adjusting
	//if (time_elapsed > time_for_char) {
	//	time_elapsed = 0.0;
	handle_input();
	//}
	// falling speed adjusting
	if (fall_time > fall_delay) {
		fall_time = 0;

		this->tryBrickFall();


	}
	// collsion check

	
	string_blink_color += 0.001f;
	
	if (string_blink_color >1.0) {
		string_blink_color = 0;
	}

	//update brick3d angle
	brick3d_angle += 0.1f;
	if (brick3d_angle > 720) {
		brick3d_angle = 0.0f;
	}
}

void CGameEngine::tryBrickFall() {
	int lines_removed = 0;
		if (!well->check_collison(last_brick, TO_DOWN)) {
			last_brick->fall();
		} else {
			PlaySound("782_clickverb.wav", NULL, SND_ASYNC);
		
			well->copy_brick_mask_to_position(last_brick);
			lines_removed = well->check_lines();
			if (lines_removed > 0) {
				//PlaySound("47341_squash.wav", NULL, SND_ASYNC);
				PlaySound("39562_click.wav", NULL, SND_ASYNC);
				player.update_cleared_lines(lines_removed);
				//player.add_score(10 + (lines_removed-1)*20);
				//std::cout<< " SCORE : " << player.get_score() << std::endl;
				player.line_combo(true);
			} else {
				player.line_combo(false);
			}

			// increase blocks used by player
			player.add_bricks_used();
			// give me new brick
			delete last_brick;
			//delete shadow_brick;
			last_brick = well->add_brick();
			next_brick = well->get_next_brick();

			if (well->check_collison(last_brick, TO_DOWN)) {
				game_over = true;
			}

		}
}

void CGameEngine::reset_shadow_brick() {
}

void CGameEngine::print_fps() {
	std::string fps_txt= "FPS :   " + std::to_string((_ULonglong)FPS);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glRasterPos2f(5, 13);
	txt->print(fps_txt.c_str());
}

void CGameEngine::draw() {

	draw_brick3d();
	//this->draw_axis();
	//well->set_position(-2.0f, -6.0f);
	this->draw_axis();


	//this->draw_bitmap();
	this->print_fps();

	glColor3f(0.5f, 0.5f, 0.5f);
	
	well->draw();
	glColor3f(1.0f, 1.0f, 1.0f);

	//for (obj_list_it it = objects.begin(); it != objects.end(); ++it) {
	//	(*it)->draw();
	//}
	
	next_brick->draw();

	last_brick->draw();
	/*
	std::string draw_score = "SCORE :   " + std::to_string((_ULonglong)player.get_score());

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glRasterPos2f(-9.5, 13);
	txt->print(draw_score.c_str());
	*/
	player.draw();

	/*
	std::string info_str;
	if (last_brick->close_to_left_wall()) {
		info_str = "close to left wall";
	}
	if (last_brick->close_to_right_wall()) {
		info_str = "close to right wall";
	}
	

	glRasterPos2f(-9, 3);
	txt->print(info_str.c_str());
	*/
	

	
	
	std::string info_str = "LEVEL UP !";
	glColor3f(string_blink_color,string_blink_color,string_blink_color);
	glRasterPos2f(-9, 3);
	txt->print(info_str.c_str());
	// reset color
	glColor3f(1,1,1);

	std::string speed_str = "char speed : " + std::to_string((_ULonglong)char_per_sec);

	glRasterPos2f(-9, 1);
	speed_txt->print(speed_str.c_str());

	if (game_over) {
		glRasterPos2f(-9.5, 12);
		txt2->print("Game Over");
	}

	if (game_pause) {
		glRasterPos2f(-9.5, 12);
		txt2->print("Game Paused");
	}
	//glColor3f(1.0f, 1.0f, 1.0f);
	//glPushMatrix();
	//glTranslatef(1.0f,1.0f,-10.0f);
	//glRasterPos2f(10, 10);
	//glPopMatrix();
}

void CGameEngine::handle_input() {
	if (!game_pause) {
#ifdef _USE_DIRECT_INPUT_8_
	if (CKeyboardHandler::inst()->keyDown(DIK_UP)) {
		last_brick->rotate(TO_RIGHT);
	}
	if (CKeyboardHandler::inst()->keyDown(DIK_DOWN)) {
		this->tryBrickFall();
	}
	if (CKeyboardHandler::inst()->keyDown(DIK_LEFT)) {
		last_brick->move_left();
	}

	if (CKeyboardHandler::inst()->keyDown(DIK_RIGHT)) {
		last_brick->move_right();
	}
	if (CKeyboardHandler::inst()->keyDown(DIK_SPACE)) {
		// change brick
		while (!well->check_collison(last_brick, TO_DOWN)) {
			this->tryBrickFall();
		}
	}
#else

	if (CKeyboardHandler::inst()->upArrowPressed()) {
		last_brick->rotate(TO_RIGHT);
	}
	if (CKeyboardHandler::inst()->downArrowPressed()) {
		this->tryBrickFall();
	}
	if (CKeyboardHandler::inst()->leftArrowPressed()) {
		last_brick->move_left();
	}

	if (CKeyboardHandler::inst()->rightArrowPressed()) {
		last_brick->move_right();
	}
	if (CKeyboardHandler::inst()->spacePressed()) {
		// change brick
		while (!well->check_collison(last_brick, TO_DOWN)) {
			this->tryBrickFall();
		}
	}
#endif
	} // if
#ifdef _USE_DIRECT_INPUT_8_
	if (CKeyboardHandler::inst()->keyDown(DIK_ESCAPE)) {
		game_over = true;
	}
#else
	if (CKeyboardHandler::inst()->escapePressed()) {
		game_pause = true;
		CGameManager::inst()->changeGameState(GameState_Menu);
		
		//game_over = true;
	}
#endif
	if (GetAsyncKeyState(VK_TAB)) {
		//well->test_print();
		well->reset();
	}
	if (GetAsyncKeyState(VK_OEM_MINUS)) {
		//well->test_print();
		char_per_sec--;
	}
	if (GetAsyncKeyState(VK_OEM_PLUS)) {
		char_per_sec++;
	}
	if (GetAsyncKeyState(VK_F5)) {
		last_brick->test_print();
	}
	//if (GetAsyncKeyState(VK_RETURN)) {
	if (CKeyboardHandler::inst()->enterPressed()) {
		game_pause = !game_pause;
		/*
		if (!game_pause) {
			game_pause = true;
		} else {
			game_pause = false;
		}
		*/
	}

}

void CGameEngine::add_brick() {
	
	//last_brick = new CBrick(well);
	//objects.push_back(last_brick);
}

void CGameEngine::setup() {

	//this->init_bmp("blue_brick.bmp");
	this->init_bmp("grey_brick.bmp");

	// setup light
	GLfloat LightAmbient[]=  { 0.5f, 0.5f, 0.5f, 1.0f };    // Ambient Light Values
	GLfloat LightDiffuse[]=  { 1.0f, 1.0f, 1.0f, 1.0f };    // Diffuse Light Values
	GLfloat LightPosition[]= { 0.0f, 0.0f, -50.0f, 1.0f };    // Light Position

	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);		// Setup The Ambient Light
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);		// Setup The Diffuse Light
	glLightfv(GL_LIGHT1, GL_POSITION,LightPosition);	// Position The Light
	glEnable(GL_LIGHT1);

	// setup texture

	//loadGLTextures();
	
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp_info.biWidth,
	bmp_info.biHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp_data);

	
	glBindTexture(GL_TEXTURE_2D, 0);

	well = new CWell;
	//objects.push_back(well);
	well->set_texture(texture);

	last_brick = well->add_brick();
	next_brick = well->get_next_brick();
	//shadow_brick = well->get_shadow_brick();
	//objects.push_back(last_brick);
	
	// kwadrygi
	quadric = gluNewQuadric();
	gluQuadricNormals(quadric, GLU_SMOOTH);
}

void CGameEngine::cleanup() {
	obj_list_it it;
	for (it = objects.begin(); it != objects.end(); ++it) {
		if ( *it != NULL) {
			delete (*it);
			*it = NULL;
		}
	}

	gluDeleteQuadric(quadric);

	if (last_brick != NULL) {
		delete last_brick;
		last_brick = NULL;
	}
	if (next_brick != NULL) {
		delete next_brick;
		next_brick = NULL;
	}
}

void CGameEngine::init_brick(unsigned int aspect, CBrick * brick) {
	switch (aspect) {
		// _|_
		case 1 :  {

		} break;
		// _|
		case 2 : {
		} break;
		// []
		case 3 : {

		} break;
		// | - kurwa, ten klocek jest dlugi na 4 ?!
		case 4 : {
		} break;
		// _/^
		case 5 : {
		} break;
		// ^\_
		case 6 : {
		} break;
	
		// L - brick
	default : {
/*		bool brick_mask[4][3][3] = {{	1, 0, 0,
									1, 0, 0,
									1, 1, 0 },
			{ 1,1,1,
			  1,0,0,
			  0,0,0},
			{ 1,1,0,
			  0,1,0,
			  0,1,0},
			{ 0,0,1,
			  1,1,1,
			  0,0,0}};
		brick->load_layouts(brick_mask);*/
	}
	}
}

void CGameEngine::draw_axis() {
	GLfloat l = 1.0;
	

  glLineWidth(2);
  //Point3d axisGLCoords; // wspolrzedne etykiety osi w GL
  //QPoint axisWidgetCoords; // wspolrzedne etykiety osi w ukladzie widgeta
  float matAmbX[] = {1.0f,0.0f,0.0f,0.9f};
  float matAmbY[] = {0.0f,1.0f,0.0f,0.9f};
  float matAmbZ[] = {0.0f,0.0f,1.0f,0.9f};
  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, matAmbX);
  // os X
  glBegin(GL_LINES);
  glColor3f(1,0,0);
  glVertex3f(0, 0, 0); glVertex3f(l, 0, 0);
  glEnd();
  // etykieta
  //  text2gl(1,0.06,0,"X");
  //  axisGLCoords.set(1, 0.06, 0); // wspolrzedne etykiety osi X w GL
  //  axisWidgetCoords = gl2screen(axisGLCoords);
  //  renderText(axisWidgetCoords.x(),axisWidgetCoords.y(),"X");
  glPushMatrix();
  glTranslatef(1, 0, 0);
  glRotatef(90, 0, 1, 0);
  gluCylinder(quadric, 0.02, 0, 0.06, 6, 4);
  glPopMatrix();
  // os Y
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, matAmbY);
  glBegin(GL_LINES);
  glColor3f(0,1,0);
  glVertex3f(0, 0, 0); glVertex3f(0, l, 0);
  glEnd();
  //  text2gl(0.06,1,0,"Y");
//   axisGLCoords.set(0.06,1,0);
//   axisWidgetCoords = gl2screen(axisGLCoords);
//   renderText(axisWidgetCoords.x(),axisWidgetCoords.y(),"Y");
  glPushMatrix();
  glTranslatef(0, 1, 0);
  glRotatef(-90, 1, 0, 0);
  gluCylinder(quadric, 0.02, 0, 0.06, 6, 4);
  glPopMatrix();
  // os Z
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, matAmbZ);
  glBegin(GL_LINES);
  glColor3f(0,0,1);
  glVertex3f(0, 0, 0); glVertex3f(0, 0, l);
  glEnd();
  //  text2gl(0,0.06,1,"Z");
//   axisGLCoords.set(0,0.06,1);
//   axisWidgetCoords = gl2screen(axisGLCoords);
//   renderText(axisWidgetCoords.x(),axisWidgetCoords.y(),"Z");
  glPushMatrix();
  glTranslatef(0, 0, 1);
  gluCylinder(quadric, 0.02, 0, 0.06, 6, 4);
  //glColor3f(1,1,1);
  glPopMatrix();

  glPopMatrix();
} // drawAxis

unsigned char * CGameEngine::LoadBitmapFile(char * filename, BITMAPINFOHEADER *bitmapInfoHeader) {
	FILE * filePtr;
	BITMAPFILEHEADER bitmapFileHeader;
	unsigned char * bitmapImage;
	unsigned int imageIdx = 0;
	unsigned char tempRGB;

	filePtr = fopen(filename, "rb");
	if (filePtr == NULL) {
		return NULL;
	}

	fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);

	if (bitmapFileHeader.bfType != BITMAP_ID) {
		fclose(filePtr);
		return NULL;
	}

	fread(bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);

	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	bitmapImage = (unsigned char*) malloc(bitmapInfoHeader->biSizeImage);

	if (!bitmapImage) {
		free(bitmapImage);
		fclose(filePtr);
		return NULL;
	}

	fread(bitmapImage, 1, bitmapInfoHeader->biSizeImage, filePtr);

	if (bitmapImage == NULL) {
		fclose(filePtr);
		return NULL;
	}

	// exchange R and B color component
	for (imageIdx = 0; imageIdx < bitmapInfoHeader->biSizeImage; imageIdx += 3) {
		tempRGB = bitmapImage[imageIdx];
		bitmapImage[imageIdx] = bitmapImage[imageIdx + 2];
		bitmapImage[imageIdx+2] = tempRGB;
	}

	fclose(filePtr);
	return bitmapImage;
}

void CGameEngine::init_bmp(char * bmp_name) {
	bmp_data = LoadBitmapFile(bmp_name, &bmp_info);
}

void CGameEngine::draw_bitmap() {
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glRasterPos2i(-9,7);
	glDrawPixels(bmp_info.biWidth, bmp_info.biHeight, GL_RGB, GL_UNSIGNED_BYTE, bmp_data);

}
/*
void CGameEngine::loadGLTextures() {
	HBITMAP hBmp;                                       // Handle Of The Bitmap
    BITMAP  bitmap;   

	byte    texture_id[]={ IDB_BITMAP1 };

	//glGenTextures(sizeof(texture), &texture[0]);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	//for (int loop=0; loop<sizeof(Texture); loop++) {
	//hBmp=(HBITMAP)LoadImage(GetModuleHandle(NULL),MAKEINTRESOURCE(texture_id[0]), 
	hBmp=(HBITMAP)LoadImage(GetModuleHandle(NULL),MAKEINTRESOURCE(texture_id[0]), 
		IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	 if (hBmp) {
		GetObject(hBmp,sizeof(bitmap), &bitmap);
		
		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		glBindTexture(GL_TEXTURE_2D, texture_id[0]);

		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bitmap.bmWidth,
			bitmap.bmHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, bitmap.bmBits);
 
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, BMP.bmWidth, BMP.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, BMP.bmBits);
		
		DeleteObject(hBmp);
	}
	

}
*/
void CGameEngine::draw_brick3d() {
	const int vert_size = 36;
	float cube_verts[vert_size][3] = {
		/* front wall */
		{1.0, 0.0, -1.0},
		{0.0, 0.0, -1.0},
		{1.0, 1.0, -1.0},
		{0.0, 0.0, -1.0},
		{0.0, 1.0, -1.0},
		{1.0, 1.0, -1.0},

		/* right wall */
		{1.0, 0.0, -1.0},
		{1.0, 1.0, 0.0},
		{1.0, 0.0, 0.0},
		{1.0, 0.0, -1.0},
		{1.0, 1.0, -1.0},
		{1.0, 1.0, 0.0},

		/* back wall */
		{1.0, 0.0, 0.0},
		{0.0, 0.0, 0.0},
		{1.0, 1.0, 0.0},
		{0.0, 0.0, 0.0},
		{0.0, 1.0, 0.0},
		{1.0, 1.0, 0.0},

		/* left wall */
		{0.0, 0.0, -1.0},
		{0.0, 1.0, 0.0},
		{0.0, 0.0, 0.0},
		{0.0, 0.0, -1.0},
		{0.0, 1.0, -1.0},
		{0.0, 1.0, 0.0},
		 
		/* bottom wall */
		{1.0, 0.0, -1.0},
		{0.0, 0.0, -1.0},
		{1.0, 0.0, 0.0},
		{0.0, 0.0, -1.0},
		{0.0, 0.0, 0.0},
		{1.0, 0.0, 0.0},

		/* top wall */
		{1.0, 1.0, -1.0},
		{0.0, 1.0, -1.0},
		{1.0, 1.0, 0.0},
		{0.0, 1.0, -1.0},
		{0.0, 1.0, 0.0},
		{1.0, 1.0, 0.0}
	};
	glColor3f(0.5f, 0.6f, 0.7f);

	glPushMatrix();
	glTranslatef(-5.0,4.0,1.0);
	glRotatef(brick3d_angle, -0.5, 1.0, -0.0);
	
	glBegin(GL_TRIANGLES);
	for (int i=0; i<vert_size; i++) {
		glVertex3fv(cube_verts[i]);
	}

	glEnd();
	glPopMatrix();
}
