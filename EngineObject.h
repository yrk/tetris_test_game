#ifndef ENGINE_OBJECT_H
#define ENGINE_OBJECT_H

#include <Windows.h>
#include <gl/GL.h>

class CEngineObject
{
protected:
	GLfloat _glx, _gly;
	GLfloat default_color[3];

public:
	CEngineObject(void);
	~CEngineObject(void);

	void set_position(GLfloat _x, GLfloat _y);
	GLfloat x();
	GLfloat y();
	virtual void draw() = 0;
	//void move_to(GLfloat x, GLfloat y) {}
	void set_default_color(GLfloat r, GLfloat g, GLfloat b);
	void set_default_color(GLfloat col[3]);
};

#endif