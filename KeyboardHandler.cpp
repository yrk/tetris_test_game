#include "KeyboardHandler.h"
#include "GameManager.h"

#include <sstream>
#include <iostream>
#include <Windows.h>

#ifdef _USE_DIRECT_INPUT_8_
bool CKeyboardHandler::init() {
	if (FAILED(DirectInput8Create(CWindowGL::inst()->getHINSTANCE(),
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(void **)&directInput,
		NULL))) {
		return false;
	}

	if (FAILED(directInput->CreateDevice(GUID_SysKeyboard, &diDevice, NULL))) {
		return false;
	}
	if (FAILED(diDevice->SetDataFormat(&c_dfDIKeyboard))) {
		return false;
	}
	if (FAILED(diDevice->SetCooperativeLevel(CWindowGL::inst()->getHWND(),
		DISCL_FOREGROUND | DISCL_NONEXCLUSIVE ))) {
			return false;
	}
	if (FAILED(diDevice->Acquire())) {
		return false;
	}
	clear_keys();

	return true;
}

bool CKeyboardHandler::keyPressed(int key) { 

}
#endif

bool CKeyboardHandler::upArrowPressed() {
	return keyPressed(upArrowKey);
	/*
	if (keyTypeTab[upArrowKey] > keyboardDelay) {
		keyTypeTab[upArrowKey] = 0;
		return true;
	}
	return false;
	*/
}
bool CKeyboardHandler::downArrowPressed() { return keyPressed(downArrowKey); }
bool CKeyboardHandler::leftArrowPressed() { return keyPressed(leftArrowKey); }
bool CKeyboardHandler::rightArrowPressed() { return keyPressed(rightArrowKey); }

bool CKeyboardHandler::enterPressed() { return keyPressed(enterKey); }
bool CKeyboardHandler::spacePressed() { return keyPressed(spaceKey); }
bool CKeyboardHandler::escapePressed() { return keyPressed(escapeKey); }

bool CKeyboardHandler::keyPressed(TKeyType keyType) {
	/*
	if (keyTypeTab[keyType].fistPress) {
		keyTypeTab[keyType].fistPress = false;
		return true;
	}
	
	if (keyTypeTab[keyType].delay > keyboardDelay) {
		keyTypeTab[keyType].delay = 0.0f;
		return true;
	}
	*/
	if (superKeys[keyType].actOnKey) {
		return true;
	}
	
	return false;
}

bool CKeyboardHandler::update() {

#ifdef _USE_DIRECT_INPUT_8_
	diDevice->Poll();
	if (FAILED(diDevice->GetDeviceState(sizeof(_keys),(LPVOID)_keys))) {
		if (FAILED(diDevice->Acquire())) {
			return false;
		}
	}
	if (FAILED(diDevice->GetDeviceState(sizeof(_keys),(LPVOID)_keys))) {
		return false;
	}

	for (unsigned int i = 0; i<256; ++i) {
		if (keyDown(i)) {
			// increase key timer
		} else {
			// reset key timer
		}
	}
#else

	unsigned int winKeyTypes[] = { VK_UP, 
								   VK_DOWN, 
								   VK_LEFT, 
								   VK_RIGHT,
								   VK_RETURN,
								   VK_SPACE,
								   VK_ESCAPE

	};

	float dt = CGameManager::inst()->getDeltaTime();
	/*
	for (unsigned int i = 0; i<KeyTypeSize; ++i) {
		if (GetAsyncKeyState(winKeyTypes[i]) & 0x8000) {
			//if (!keyTypeTab[i].fistPress && keyTypeTab[i].delay <= 0.01f) {
			//	keyTypeTab[i].fistPress = true;
			//}
			keyTypeTab[i].delay += dt;
		} 
		else {
			keyTypeTab[i].fistPress = false;
			keyTypeTab[i].delay = 0.0f;

		}
	}
	*/
	for (unsigned int i = 0; i<KeyTypeSize; ++i) {
		if (GetAsyncKeyState(winKeyTypes[i]) & 0x8000) {
			if (superKeys[i].keyDown) {
				superKeys[i].actOnKey = false;
				superKeys[i].timeDown += dt + superKeys[i].velocity*dt;
				// limit cap velocity rise
				if (superKeys[i].velocity < 3) {
					superKeys[i].velocity += 0.2f;
				}
				//if (superKeys[i].timeDown > 0.07f) {
				if (superKeys[i].timeDown > 0.407f) {
					superKeys[i].actOnKey = true;
					superKeys[i].timeDown = 0.0f;
				}

			} else {
				superKeys[i].keyDown = true;
				superKeys[i].actOnKey = true;
			}
			
		} 
		else {
			// reset key status
			superKeys[i].keyDown = false;
			superKeys[i].actOnKey = false;
			superKeys[i].timeDown = 0.0f;
			superKeys[i].velocity = 0.0f;
		}
	}
#endif
	return true;
}

CKeyboardHandler::CKeyboardHandler(void) {
	for (unsigned int i = 0; i< KeyTypeSize; ++i) {
		keyTypeTab[i].fistPress = false;
		keyTypeTab[i].delay = 0.0f;
	}

	keyboardDelay = 0.07f;

	ZeroMemory(superKeys, 256*sizeof(DIKeyDescStruct));
}


CKeyboardHandler::~CKeyboardHandler(void) {
#ifdef _USE_DIRECT_INPUT_8_
	if (diDevice) {
		diDevice->Unacquire();
		diDevice->Release();
	}
#endif
}

CKeyboardHandler * CKeyboardHandler::inst() {
	if (!khInstance) {
		khInstance = new CKeyboardHandler();
#ifdef _USE_DIRECT_INPUT_8_
		khInstance->init();
#endif
	}
	return khInstance;
}

CKeyboardHandler * CKeyboardHandler::khInstance = NULL;