#include "TextPrinter.h"
#include "CWindowGL.h"

#include <gl/gl.h>

CTextPrinter::CTextPrinter() : CHAR_LIST_SIZE(96)
{
	//hDC = hdc;
	//std::cout<<"textList : "<< textList << std::endl;
}

CTextPrinter::CTextPrinter(char * fontName, int fontSize) : CHAR_LIST_SIZE(96)
{
	
	createBitmapFont(fontName, fontSize);
	//std::cout<<"textList : "<< textList << std::endl;
}

CTextPrinter::~CTextPrinter(void)
{
	clearFont(textList);
}

void CTextPrinter::print(const char * str)
{
	//std::cout<<"textList : "<< textList << std::endl;
	printString(textList, str);
}

void CTextPrinter::printString(unsigned int base, const char * str)
{
	if ((base == 0) || (str == NULL)) {
		//std::cout<<"base : "<< base << std::endl;
		//std::cout<<"str : "<< str << std::endl;
		return;
	}

	glPushAttrib(GL_LIST_BIT);
	glListBase(base-32);
	
	glCallLists(strlen(str), GL_UNSIGNED_BYTE, str);
	glPopAttrib();
}


void CTextPrinter::createBitmapFont(char * fontName, int fontSize)
{
	//std::cout<< "max display lists : "<<

	textList = glGenLists(CHAR_LIST_SIZE);
	//int dupa = glGenLists(CHAR_LIST_SIZE);

	if (stricmp(fontName, "symbol") == 0) {
		hFont = CreateFont(fontSize, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
			SYMBOL_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, 
			ANTIALIASED_QUALITY, FF_DONTCARE | DEFAULT_PITCH, 
			fontName);
	}
	else {
		hFont = CreateFont(fontSize, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
			ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, 
			ANTIALIASED_QUALITY, FF_DONTCARE | DEFAULT_PITCH, 
			fontName);
	}

	if (!hFont) {
		std::cout<<"cos sie rozkurwilo !!!"<<std::endl;
		return;
	}
	HDC hDC = CWindowGL::inst()->hdc();

	SelectObject(hDC, hFont);
	wglUseFontBitmaps(hDC, 32, CHAR_LIST_SIZE, textList);

	//return textList;
}

void CTextPrinter::clearFont(unsigned int base)
{
	if (base != 0) {
		glDeleteLists(base, 96);
	}
}
