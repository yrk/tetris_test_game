#include "CWindow.h"
#include "CWindowGL.h"

#include "Brick.h"

#include <cstdio>
#include <io.h>
#include <fcntl.h>

#define WIN32_LEAN_AND_MEAN

//CWindowGL window;

void setupConsole() {
	AllocConsole();

    HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
    int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
    FILE* hf_out = _fdopen(hCrt, "w");
    //setvbuf(hf_out, NULL, _IONBF, 1);
	setvbuf(hf_out, NULL, _IONBF, 0);
    *stdout = *hf_out;

    HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
    hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
    FILE* hf_in = _fdopen(hCrt, "r");
    setvbuf(hf_in, NULL, _IONBF, 128);
    *stdin = *hf_in;

	//setvbuf(stdout, NULL, _IONBF, 0);

    // use the console just like a normal one - printf(), getchar(), ...
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, 
	LPSTR lpCmdLine, int nCmdShow)
{
	POINT windowPosition = {200, 100};
	POINT windowSize = {800, 600};

	// do not use console :)
	//setupConsole();
	
	if (!CWindowGL::inst()->Init(hInstance, windowPosition, windowSize))
	{
		const char * appText = "ERROR: !window.Init(hInstance, windowPosition, windowSize)";
		const char * appCaption = "Ups ! Mamy problem...";
		
		MessageBox(NULL, appText, appCaption, MB_OK | MB_ICONWARNING);
		return EXIT_FAILURE;
	}
	else return CWindowGL::inst()->run(); 

	return 0;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	return CWindowGL::inst()->WndProc(hWnd, message, wParam, lParam);
}