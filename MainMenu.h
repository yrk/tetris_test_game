#ifndef _MAIN_MENU_H_
#define _MAIN_MENU_H_

#include "TextPrinter.h"

#include <string>

enum MenuValues {
	Menu_Continue_Game = 1,
	Menu_New_Game,
	Menu_Load_Game,
	Menu_Save_Game,
	Menu_HiScores,
	Menu_Exit,
	Menu_Size
};

typedef struct TMenuItemStruct {
	MenuValues value;
	//unsigned int position;
	std::string name;
} TMenuItem;

class CMainMenu
{

public :
	void draw();
	void update();
	MenuValues get_value();
	void cursor_down();
	void cursor_up();

	void setTextPrinter(CTextPrinter * txt);

public:
	CMainMenu();
	~CMainMenu(void);

private:
	void setup();

private:
	CTextPrinter * txt_info;
	MenuValues current_value;
	unsigned int current_position;
	//TMenuItem menu[Menu_Size+1];
	TMenuItem menu[7];
};

#endif

