#ifndef CWINDOW_H
#define CWINDOW_H

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


class CWindow
{
protected:
	POINT userSpaceSize;
	bool appIsRunning;

	bool bUseQPC;
	LARGE_INTEGER uFreq;
	LARGE_INTEGER uTicks;

	double time_app_is_running;
	HWND _hWnd;
	HINSTANCE _hInstance;

public:
	CWindow() : appIsRunning(true),
				time_app_is_running(0.0)
	{
		bUseQPC = (QueryPerformanceFrequency (&uFreq) != 0);




	}
	~CWindow() {
//
	}
	LRESULT WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	bool Init(HINSTANCE appHandle, POINT windowPosition, POINT windowSize);
	bool ChangeDisplayResolution(long width, long height, long colorDepth);
	RECT getDesktopSize();

	float getSeconds();
	WPARAM run();
	WPARAM RunEvents();

	HWND getHWND() { return _hWnd; }
	HINSTANCE getHINSTANCE() { return _hInstance; }

};


#endif